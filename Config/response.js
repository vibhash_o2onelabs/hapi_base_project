


const sendSuccessMessage=(reply,message,data)=>{
    reply ({
        status:200,
        message:message,
        data:data
    })
};


const sendErrorMessage=(reply,code)=>{
    reply ({
        status:code,
        message:'Internal Server Error',

    })
};

const sendErrorCustomMessage=(reply,code,message)=>{
    reply ({
        status:code,
        message:message,

    })
}

module.exports = {
    sendSuccessMessage,
    sendErrorMessage,
    sendErrorCustomMessage

}