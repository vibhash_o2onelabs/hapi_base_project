

const Config             =  require('../Config'),
      async              =  require('async');



const fcm = new FCM(Config.pushConfig.serverKey);





const sendPushMulticast=function(dtoken,Msg,Data,callback){

    async.auto({
        sendPush:function(cb){
            const message = {
                registration_ids: dtoken,
                notification: {
                    title: Msg.TITLE,
                    body: Msg.BODY
                },
                data:Data,
                priority: 'high'
            };
            fcm.send(message, function(err, result){
                if (err) {
                    console.log("Something has gone wrong!",err);
                    callback(err);
                } else {
                    console.log("Successfully sent with response: ", result);
                    cb(null,result);
                }
            });
        }
    },function(err,result){
        callback(err,result);
    })
};






module.exports = {

    sendPushMulticast  : sendPushMulticast,
  //  sendNormalSMSToUser: sendNormalSMSToUser,

};