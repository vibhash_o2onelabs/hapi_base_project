'use strict';

const Config = require('../Config'),
    Jwt = require('jsonwebtoken'),
    DOAManager = require('../DAOManager').queries,
    Models = require('../Models');


const getTokenFromDB = async function (userId, userType, token, userTokenType, field) {

    console.log("=============getTokenFromDB======================================", userTokenType, userType, token);
    let criteria = {
        _id: userId
    };

    criteria[field] = token;

    let data;

    if (userType == 'USERS' && userType == userTokenType)
        data = await DOAManager.getData(Models.Users, {user_token:token}, {}, {lean: true})
    else if (userType == 'EDITORS' && userType == userTokenType) {
        data = await DOAManager.getData(Models.Editors, criteria, {}, {lean: true});
    }

    if (data && data.length) {
        data[0].userType = userType;
        data[0].accessToken = token;
        return Promise.resolve(data[0]);
    } else {
        if (field == Config.APP_CONSTANTS.DATABASE.TOKEN_FIELDS.ACCESS_TOKEN)
            return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
        else if (field == Config.APP_CONSTANTS.DATABASE.TOKEN_FIELDS.EMAIL_VERIFICATION)
            return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_VERIFICATION_TOKEN);
        else
            return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    }
};

const setTokenInDB = async function (userId, userType, tokenToSave, field) {
    let criteria = {
        _id: userId
    };
    let setQuery = {};

    setQuery[field] = tokenToSave;

    let data;

    if (userType === Config.APP_CONSTANTS.DATABASE.USER_ROLES.USERS)
        data = await DOAManager.findAndUpdate(Models.Users, criteria, {$addToSet: {accessToken: tokenToSave}}, {new: true});
    else if (userType === Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN)
        data = await DOAManager.findAndUpdate(Models.Admins, criteria, setQuery, {new: true});
    else if (userType === Config.APP_CONSTANTS.DATABASE.USER_ROLES.EDITORS)
        data = await DOAManager.findAndUpdate(Models.Editors, criteria, setQuery, {new: true});


    if (data && Object.keys(data).length){
        console.log('ssssssssssssssssssssssssssssssssssssssssssssssssss',data)
        return Promise.resolve(true)
    } else{
        console.log('2333333333333333333333333333333333333333')
        return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    }


};

const expireTokenInDB = async function (payloadData) {
    let criteria = {
        _id: payloadData._id
    };
    let setQuery = {
        $unset: {
            accessToken: 1,
            online: false
        }
    };

    let data;

    if (payloadData.userType === Config.APP_CONSTANTS.DATABASE.USER_ROLES.fireFighter)
        data = await DOAManager.findAndUpdate(Models.FireFighters, criteria, {$pull: {accessToken: payloadData.accessToken}}, {new: true});
    else if (payloadData.userType === Config.APP_CONSTANTS.DATABASE.USER_ROLES.VENDOR)
        data = await DOAManager.findAndUpdate(Models.Vendor, criteria, setQuery, {new: true});
    else if (payloadData.userType === Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN)
        data = await DOAManager.findAndUpdate(Models.Admins, criteria, setQuery, {new: true});

    if (data && Object.keys(data).length)
        return true;
    else
        return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
};

const verifyToken = async function (token, uerType, field) {
    console.log(token, uerType, field);
    let decoded = await Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
    if (decoded.id && decoded.type)
        return await getTokenFromDB(decoded.id, decoded.type, token, uerType || decoded.type, field);
    else{

        return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
    }

};

const setToken = async function (tokenData, field) {
    if (!tokenData.id || !tokenData.type) {
        return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        let tokenToSend = await Jwt.sign(tokenData, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
        await setTokenInDB(tokenData.id, tokenData.type, tokenToSend, field);
        return Promise.resolve({accessToken: tokenToSend});
    }

};

const expireToken = async function (token) {
    let decoded = await Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
    if (decoded.id && decoded.type)
        return await expireTokenInDB(decoded.id, decoded.type);
    else
        return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
};

const decodeToken = function (token) {
    return Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
};

const generateToken = function (tokenData) {
    return Jwt.sign(tokenData, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
}

module.exports = {
    expireTokenInDB: expireTokenInDB,
    expireToken: expireToken,
    setToken: setToken,
    verifyToken: verifyToken,
    decodeToken: decodeToken,
    generateToken: generateToken
};