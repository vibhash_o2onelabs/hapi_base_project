const mongoose     =  require('mongoose'),
    Schema         =  mongoose.Schema,
    Config         =  require('../Config'),
    moment = require('moment-timezone'),
    time    = moment.tz("Asia/Baghdad");




const Bookmarks = new Schema({
    user:{type: mongoose.Schema.ObjectId, ref: 'Users'},
    stories_id:{type: mongoose.Schema.ObjectId, ref: 'Stories'},
    is_deleted: { type: Boolean, required: true,default:false},
    created_on: { type: String, required:true,default:time.format()},
}, { versionKey: false });



module.exports = mongoose.model('Bookmarks',Bookmarks);