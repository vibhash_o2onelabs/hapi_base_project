
const Controller          =  require('../Controllers'),
      UniversalFunctions  =  require('../Utils/UniversalFunctions'),
      Joi                 =  require('joi'),
      Config              =  require('../Config');

let AuthRoutes = [

    {
        method: 'POST',
        path: '/api/admin/addFireFighters',
        handler: function (request, reply) {
            Controller.AdminController.addFireFighters(request.payload)
                .then(result =>{
                    reply(UniversalFunctions.sendSuccess(result));
                })
                .catch(reason =>{
                    reply(UniversalFunctions.sendError(reason));
                })
        },
        config: {
            description: 'add Fire Fighters  API ...',
            auth:'AdminAuth',
            tags: ['api','admin'],
            validate: {
                payload: {
                    email              : Joi.string().trim().email().required().lowercase().label('email'),
                    firstName          : Joi.string().trim().required(),
                    lastName           : Joi.string().trim().required(),
                    batchId            : Joi.string().trim().required(),
                    password           : Joi.string().trim().required(),
                    company            : Joi.string().trim().required(),
                },
                failAction : UniversalFunctions.failActionFunction,
                headers    : UniversalFunctions.authorizationHeaderObj,
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/addCompanies',
        handler: function (request, reply) {
            Controller.AdminController.addCompanies(request.payload)
                .then(result =>{
                    reply(UniversalFunctions.sendSuccess(result));
                })
                .catch(reason =>{
                    reply(UniversalFunctions.sendError(reason));
                })
        },
        config: {
            description: 'add Companies  API ...',
            auth:'AdminAuth',
            tags: ['api','admin'],
            validate: {
                payload: {
                    email              : Joi.string().trim().email().required().lowercase().label('email'),
                    company            : Joi.string().trim().required(),
                },
                failAction : UniversalFunctions.failActionFunction,
                headers    : UniversalFunctions.authorizationHeaderObj,
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/forwardRequestToPlumber',
        handler: function (request, reply) {
            Controller.AdminController.forwardRequestToPlumber(request.payload)
                .then(result =>{
                    reply(UniversalFunctions.sendSuccess(result));
                })
                .catch(reason =>{
                    reply(UniversalFunctions.sendError(reason));
                })
        },
        config: {
            description: 'forward Request To Plumber  API ...',
            auth:'AdminAuth',
            tags: ['api','admin'],
            validate: {
                payload: {
                    hydrantId : Joi.string().trim().required(),
                    plumberIds: Joi.array().required().items(Joi.string().required())
                },
                failAction : UniversalFunctions.failActionFunction,
                headers    : UniversalFunctions.authorizationHeaderObj,
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/getPlumbingCompanies',
        handler: function (request, reply) {
            Controller.AdminController.getPlumbingCompanies(request.query)
                .then(result => {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT,result));
                })
                .catch(reason => {
                    reply(UniversalFunctions.sendError(reason));
                })
        },
        config: {
            description: ' get Plumbing Companies firefighter ...',
            auth:'AdminAuth',
            tags: ['api','admin'],
            validate: {
                query: {
                    limit              : Joi.number().required(),
                    skip               : Joi.number().required()
                },
                failAction: UniversalFunctions.failActionFunction,
                headers    : UniversalFunctions.authorizationHeaderObj,

            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/admin/getfireFighters',
        handler: function (request, reply) {
            Controller.AdminController.getfireFighters(request.query)
                .then(result => {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT,result));
                })
                .catch(reason => {
                    reply(UniversalFunctions.sendError(reason));
                })
        },
        config: {
            description: ' get fire Fighters ...',
            auth:'AdminAuth',
            tags: ['api','admin'],
            validate: {
                query: {
                    limit              : Joi.number().required(),
                    skip               : Joi.number().required()
                },
                failAction: UniversalFunctions.failActionFunction,
                headers    : UniversalFunctions.authorizationHeaderObj,

            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/blockCompany',
        handler: function (request, reply) {
            Controller.AdminController.blockCompany(request.payload)
                .then(result => {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT,result));
                })
                .catch(reason => {
                    reply(UniversalFunctions.sendError(reason));
                })
        },
        config: {
            description: ' block Company ...',
            auth:'AdminAuth',
            tags: ['api','admin'],
            validate: {
                payload: {
                    companyId          : Joi.string().trim().required(),
                    companyStatus      : Joi.string().required().valid(
                                         [
                                          Config.APP_CONSTANTS.BLOCK_USER.BLOCK,
                                          Config.APP_CONSTANTS.BLOCK_USER.UNBLOCK
                                         ])
                },
                failAction: UniversalFunctions.failActionFunction,
                headers    : UniversalFunctions.authorizationHeaderObj,

            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
];

let NonAuthRoutes = [
    {
        method: 'POST',
        path: '/api/admin/adminLogin',
        handler: function (request, reply) {
            Controller.AdminController.login(request.payload)
                .then(result =>{
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGIN, result));
                })
                .catch(reason =>{
                    reply(UniversalFunctions.sendError(reason));
                })
        },
        config: {
            description: 'Admin Login...',
            tags: ['api','admin'],
            validate: {
                payload: {
                    email:Joi.string().trim().required(),
                    password:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/admin/addCSV',
        handler: function (request, reply) {
            Controller.AdminController.addCSV(request.payload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'csv file',
            tags: ['api', 'sellers'],
            payload: {
                maxBytes: 20000000,
                parse: true,
                timeout: false,
                output: 'file'
            },
            validate: {
                payload: {
                    csvFile: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('csv file'),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    ];

module.exports=[...AuthRoutes,...NonAuthRoutes];