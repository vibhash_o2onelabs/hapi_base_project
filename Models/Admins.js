
const mongoose       =  require('mongoose'),
      Schema         =  mongoose.Schema,
      Config         =  require('../Config');


// embedded docs (generating _id )
let LogInAttempts = new Schema({
    timeStamp    : {type: Number, default: +new Date()},
    validAttempt : {type: Boolean, required: true},
    ipAddress    : {type: String, required: true}
});



let Admins = new Schema({
    name               : {type: String, trim: true, index: true, default: null, sparse: true},
    email              : {type: String, trim: true, unique: true, index: true},
    accessToken        : {type: String, trim: true, index: true, unique: true,sparse: true},
    deviceToken        : {type: String, trim: true,default:null,index: true},
    password           : {type: String, required:true},
    passwordResetToken : {type: String, trim: true, unique: true, sparse:true},
    createdDate        : {type: Number, default: Date.now,index:true,required: true},
    logInAttempts      : [LogInAttempts]
});

module.exports = mongoose.model('Admins', Admins);