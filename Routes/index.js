

const UserRoutes    =  require('./UserRoutes');
const  EditorRoutes   =  require('./EditorRoutes');


// Exporting All Routes here

module.exports = [...UserRoutes,...EditorRoutes];