module.exports = {
    Admins: require('./Admins'),
    Editors: require('./Editors'),
    Users: require('./Users'),
    Tags: require('./Tags'),
    Stories: require('./Stories'),
    NewsCategories: require('./Newscategories'),
    Bookmarks: require('./Bookmarks'),

};