
const mongoose     =  require('mongoose'),
    Schema         =  mongoose.Schema,
    Config         =  require('../Config'),
    moment = require('moment-timezone'),
   newYork    = moment.tz("Asia/Baghdad");


// enums
let deviceTypeEnum = [
    Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
    Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
];

let Editors = new Schema({
    full_name:{type:String,sparse:true},
    email_id:{type:String,required:true,unique:true},
    phone_number:{type:String,required:false},
    password:{type:String,required:true},
    photos:{type:String,sparse:true},
    accessToken: { type: String, required: false,unique:true},
    is_deleted: { type: Boolean, required: true,default:false},
    created_on: { type: String, required:true,default:newYork.format()},
}, { versionKey: false });




module.exports = mongoose.model('Editors', Editors);