'use strict';

const TokenManager         =  require('../Lib/TokenManager'),
      UniversalFunctions   =  require('../Utils/UniversalFunctions'),
      Config               =  require('../Config');

exports.register  = function(server, options, next){

//Register Authorization Plugin
    server.register(require('hapi-auth-bearer-token'), function (err) {
        server.auth.strategy('UserAuth', 'bearer-access-token', {
            allowQueryToken: false,
            allowMultipleHeaders: true,
            accessTokenName: 'accessToken',
            validateFunc: function (token, callback) {
                TokenManager.verifyToken(token,Config.APP_CONSTANTS.DATABASE.USER_ROLES.USERS,Config.APP_CONSTANTS.DATABASE.TOKEN_FIELDS.ACCESS_TOKEN)
                    .then(result =>{
                        callback(null, true, {token: token, userData: result});
                    })
                    .catch(reason => {
                        callback(null, false, {token: token, userData: null})
                    });
            }
        });
        server.auth.strategy('AdminAuth', 'bearer-access-token', {
            allowQueryToken: false,
            allowMultipleHeaders: true,
            accessTokenName: 'accessToken',
            validateFunc: function (token, callback) {
                TokenManager.verifyToken(token,Config.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN,Config.APP_CONSTANTS.DATABASE.TOKEN_FIELDS.ACCESS_TOKEN)
                    .then(result =>{
                        callback(null, true, {token: token, userData: result});
                    })
                    .catch(reason => {
                        callback({status:401,error:'Unauthorized',message: "Missing authentication"})
                    });
            }
        });

        server.auth.strategy('EditorAuth', 'bearer-access-token', {
            allowQueryToken: false,
            allowMultipleHeaders: true,
            accessTokenName: 'accessToken',
            validateFunc: function (token, callback) {
                TokenManager.verifyToken(token,Config.APP_CONSTANTS.DATABASE.USER_ROLES.EDITORS,Config.APP_CONSTANTS.DATABASE.TOKEN_FIELDS.ACCESS_TOKEN)
                    .then(result =>{
                        callback(null, true, {token: token, userData: result});
                    })
                    .catch(reason => {
                        callback({status:401,error:'Unauthorized',message: "Missing authentication"})
                    });
            }
        });
    });

    next();
};

exports.register.attributes = {
    name: 'auth-token-plugin'
};