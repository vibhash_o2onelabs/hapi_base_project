const Query = require('./../DAOManager/queries');
const Models = require('./../Models');
const Response = require('./../Config/response');
const Constants=require('./../Config/appConstants')

const EditorEntity = require('./../Entites/Editors')


const registerEditor = async (payloadData, reply) => {
    try {
        let criteria1 = {};
        criteria1.email_id = payloadData.email_id;
        criteria1.is_deleted = false;
        let check_editor_exist = await EditorEntity.editorExist(criteria1);

        if (!check_editor_exist) {
            let register_editor = await EditorEntity.registerEditor(payloadData);
            let generate_token = await EditorEntity.updateToken(register_editor);
            let criteria2 = {};
            criteria2._id = register_editor._id;
            let final_result = await EditorEntity.editorExist(criteria2)
            Response.sendSuccessMessage(reply, 'success', final_result)
        } else {
            Response.sendSuccessMessage(reply, 'Editor Already Exist', check_editor_exist)
        }
    } catch (err) {
        console.log('.................the error is....................', err);
        Response.sendErrorMessage(reply, Constants.STATUS_CODE.ERROR_500)
    }
};


const updateEditor = async (payloadData, reply) => {
    try {
        let criteria={};
        criteria._id =payloadData.userData._id
        criteria.is_deleted = false;
        let update_editor=await EditorEntity.updateEditor(payloadData);
        Response.sendSuccessMessage(reply, 'success', update_editor)

    } catch (err) {
        console.log('.................the error is....................', err);
        Response.sendErrorMessage(reply, Constants.STATUS_CODE.ERROR_500)
    }
};

const createCategory = async (payloadData, reply) => {
    try {
        console.log('.........................................',payloadData)
        Response.sendSuccessMessage(reply, 'success', payloadData)

    } catch (err) {
        console.log('.................the error is....................', err);
        Response.sendErrorMessage(reply, Constants.STATUS_CODE.ERROR_500)
    }
};


module.exports = {
    registerEditor,
    updateEditor:updateEditor,
    createCategory:createCategory
};

