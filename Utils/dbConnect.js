const mongoose      =  require('mongoose'),
      dbConfig      =  require('../Config').dbConfig;

mongoose.Promise           =     global.Promise;

let dbUrl='';

if(process.env.NODE_ENV==='test' ){
    dbUrl="mongodb://"+dbConfig.TEST.USER+":"+dbConfig.TEST.PASS+"@"+dbConfig.TEST.DB_IP+"/"+dbConfig.TEST.DB_NAME;
  console.log("==============dbUrl=========",dbUrl);
    process.env.PORT=dbConfig.TEST.PORT;
}else if(process.env.NODE_ENV==='live'){
    dbUrl="mongodb://"+dbConfig.LIVE.USER+":"+dbConfig.LIVE.PASS+"@"+dbConfig.LIVE.DB_IP+"/"+dbConfig.LIVE.DB_NAME;
    process.env.PORT=dbConfig.LIVE.PORT;
}else if(process.env.NODE_ENV==='dev'){
    dbUrl="mongodb://"+dbConfig.DEV.USER+":"+dbConfig.DEV.PASS+"@"+dbConfig.DEV.DB_IP+"/"+dbConfig.DEV.DB_NAME;
    process.env.PORT=dbConfig.DEV.PORT;
    console.log("dbUrldbUrldbUrldbUrldbUrldbUrl",dbUrl);
}else{
    dbUrl="mongodb://"+dbConfig.DEFAULT.DB_IP+"/"+dbConfig.DEFAULT.DB_NAME;
    process.env.PORT=dbConfig.DEFAULT.PORT;
}




mongoose.connect(dbUrl,{ useMongoClient: true })
    .then(result =>{
        console.log('MongoDB Connected..........',process.env.NODE_ENV && process.env.NODE_ENV || 'DEFAULT');
    })
    .catch(reason =>{
        console.log("DB Error: ", reason);
        process.exit(1);
    });