const Query=require('./../DAOManager/queries');
const Models=require('./../Models');
const Response=require('./../Config/response');
const UniversalFunctions  =  require('../Utils/UniversalFunctions');
const  TokenManager        =  require('../Lib/TokenManager');
const  Config        =  require('../Config');


const registerEditor=(payloadData)=>{
    let dataToSave={};
    dataToSave.email_id=payloadData.email_id;
    dataToSave.full_name=payloadData.full_name;
    dataToSave.email_id=payloadData.email_id;
    dataToSave.full_name=payloadData.full_name;
    dataToSave.email_id=payloadData.email_id;
    dataToSave.full_name=payloadData.full_name;
    dataToSave.password=UniversalFunctions.hashPassword(payloadData.password)
    return new Promise((resolve,reject)=>{
        Query.saveData(Models.Editors,dataToSave).then((result)=>{
            resolve(result)
        }).catch((err)=>{
            reject(err)
        })
    })
};


const editorExist=(criteria)=>{
    return new Promise((resolve,reject)=>{
        Query.findOne(Models.Editors,criteria,{},{lean:true}).then((result)=>{
            resolve(result)
        }).catch((err)=>{
            reject(err)
        })
    })
};

const updateToken=(editorData)=>{
    let criteria={
        id:editorData._id,
        type: Config.APP_CONSTANTS.DATABASE.USER_ROLES.EDITORS
    };
    return new Promise((resolve,reject)=>{
     TokenManager.setToken(criteria, Config.APP_CONSTANTS.DATABASE.TOKEN_FIELDS.ACCESS_TOKEN).then((result)=>{
         resolve(result)
     }).catch((err)=>{
         reject(err)
     })
    })
}

const updateEditor=(payloadData)=>{
    let criteria={};
    criteria._id=payloadData.userData._id;
    criteria.is_deleted=false;

    let dataToSave={};
    if(payloadData.email_id){
        dataToSave.email_id=payloadData.email_id;
    }
    if(payloadData.full_name){
        dataToSave.full_name=payloadData.full_name;
    }
    if(payloadData.phone_number){
        dataToSave.phone_number=payloadData.phone_number;
    };
    if(payloadData.password){
        dataToSave.password=UniversalFunctions.hashPassword(payloadData.password)
    };
    return new Promise((resolve,reject)=>{
        Query.findAndUpdate(Models.Editors,criteria,dataToSave,{lean:true,new:true}).then((result)=>{
            resolve(result)
        }).catch((err)=>{
            reject(err)
        })
    })
}

module.exports = {
    registerEditor:registerEditor,
    editorExist:editorExist,
    updateToken:updateToken,
    updateEditor:updateEditor

};








