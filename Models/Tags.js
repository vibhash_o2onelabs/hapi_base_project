
const mongoose     =  require('mongoose'),
    Schema         =  mongoose.Schema,
    Config         =  require('../Config'),
    moment = require('moment-timezone'),
    time    = moment.tz("Asia/Baghdad");




let Tags = new Schema({
    tag_name: {type: String, required: true},
    editor: {type: mongoose.Schema.ObjectId, ref: 'Editors'},
    is_deleted: {type: String, required: true, default: false},
    created_on: {type: String, required: true, default: time.format()},
},{ versionKey: false });




module.exports = mongoose.model('Tags', Tags);