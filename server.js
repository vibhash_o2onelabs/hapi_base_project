
'use strict';

const   Hapi               =  require('hapi'),
    Server             =  new Hapi.Server(),
    Routes             =  require('./Routes'),
    Bootstrap          =  require('./Utils/BootStrap'),
    HapiSwagger        =  require('hapi-swagger'),
    Pack               =  require('./package'),
    Plugins            =  require('./Plugins'),
    Config             =  require('./Config'),
    Token              =  require('./Lib/TokenManager'),
    DbConnect          =  require('./Utils/dbConnect'),
    Uni                =  require('./Utils/UniversalFunctions');



Server.connection({
    port:process.env.PORT,
    routes: { cors: true }
});


const hapiOptions = {
    info: {
        title: 'SeedProject API Documentation',
        version: Pack.version
    }
};


Server.register(Plugins, function(err){
    if(err){
        throw err;
    }
    Server.route(Routes);
});





Server.start(
    console.log("server is running"),
    console.log('Server running at:', Server.info.uri)
);


// Bootstrap admins................

Bootstrap.bootStrapAdmin()
    .then(data =>{
        console.log("Bootstrap for Admin is successfull......");
    })
    .catch(reason =>{
        console.log("Error occured in bootstrap admin......",reason);
    });

