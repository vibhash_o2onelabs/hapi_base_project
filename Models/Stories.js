
const mongoose     =  require('mongoose'),
    Schema         =  mongoose.Schema,
    Config         =  require('../Config'),
    moment = require('moment-timezone'),
    time    = moment.tz("Asia/Baghdad");




let Stories = new Schema({
    stories_categories:{type:String,sparse:true},
    editor:{type: mongoose.Schema.ObjectId, ref: 'Editors'},
    tag:{type:[String],sparse:true},
    description:{type:String,sparse:true},
    stories_headlines:{type:String,required:true,unique:true},
    stories_content:{type:String,required:true,unique:true},
    stories_source_link:{type:String,required:true,unique:false},
    stories_media_url:{type:[String],sparse:true},
    media_type:{type:String,sparse:true},
    stories_score:{type:Number,default:0},
    total_views:{type:Number,default:0},
    is_published:{type: Boolean, required: true,default:false},
    is_claimed: { type: Boolean, required: true,default:true},
    is_reviewed: { type: Boolean, required: true,default:false},
    is_approved:{type: Boolean, required: true,default:false},
    is_deleted: { type: Boolean, required: true,default:false},
    send_push_to_all:{type: Boolean, required: true,default:false},
    reviewed_by:[{type: mongoose.Schema.ObjectId, ref: 'Editors'}],
    created_on: { type: Number, required:true,default:0},
    published_on: { type: Number, required:false,default:0},
    timeCreated:{ type: String, required:true,default:time.format()},
},{ versionKey: false });




module.exports = mongoose.model('Stories', Stories);