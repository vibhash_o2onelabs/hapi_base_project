
const mongoose     =  require('mongoose'),
    Schema         =  mongoose.Schema,
    Config         =  require('../Config'),
    moment = require('moment-timezone'),
    newYork    = moment.tz("Asia/Baghdad");


let deviceTypeEnum = [
    Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
    Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
];

let Users = new Schema({
    device_id:{ type: String, sparse: true,unique:true},
    stories_viewed:[{type: mongoose.Schema.ObjectId, ref: 'Stories'}],
    impression_stories:[{type: mongoose.Schema.ObjectId, ref: 'Stories'}],
    device_token: { type: String, sparse: true,unique:true},
    accessToken: { type: String, required: true,unique:false},
    device_type: {type: String, enum: deviceTypeEnum,default:Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS},
    is_deleted: { type: Boolean, required: true,default:false},
    is_push_enable:{ type: Boolean, required: true,default:true},
    created_on: { type: String, required:true,default:newYork.format()},
},{ versionKey: false });




module.exports = mongoose.model('Users', Users);