const Controller = require('../Controllers'),
    UniversalFunctions = require('../Utils/UniversalFunctions'),
    Joi = require('joi'),
    Config = require('../Config');


let AuthRoutes = [

    {
        method: 'POST',
        path: '/api/editors/',
        handler: function (request, reply) {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            if (userData && userData._id && userData.userType) {
                request.payload.userId = userData._id;
            }
            Controller.EditorController.registerEditor(request.payload, reply)
        },
        config: {
            description: 'register editors',
            //  auth:'UserAuth',
            tags: ['api', 'editors'],
            validate: {
                payload: {
                    email_id: Joi.string().email({minDomainAtoms: 2}),
                    full_name: Joi.string().trim().required(),
                    phone_number: Joi.string().trim().optional(),
                    password: Joi.string().trim().required(),
                },
                failAction: UniversalFunctions.failActionFunction,
                //   headers    : UniversalFunctions.authorizationHeaderObj,

            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/editors/',
        handler: function (request, reply) {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            if (userData && userData._id && userData.userType) {
                request.payload.userData = userData
            }
            Controller.EditorController.updateEditor(request.payload, reply)
        },
        config: {
            description: 'update editor profile',
            auth: 'EditorAuth',
            tags: ['api', 'editors'],
            validate: {
                payload: {
                    full_name: Joi.string().trim().optional(),
                    phone_number: Joi.string().trim().optional(),
                    password: Joi.string().trim().optional(),
                },
                failAction: UniversalFunctions.failActionFunction,
                headers: UniversalFunctions.authorizationHeaderObj,

            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/editors/categories',
        handler: function (request, reply) {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            if (userData && userData._id && userData.userType) {
                request.payload.userData = userData
            }
            Controller.EditorController.createCategory(request.payload, reply)
        },
        config: {
            description: 'create category',
            tags: ['api', 'editors'],
            validate: {
                payload: {
                    name: Joi.string().required(),
                    category_names: Joi.array().required(),
                },
                failAction: UniversalFunctions.failActionFunction,


            },
            plugins: {
                'hapi-swagger': {
                    //   payloadType : '',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

];

module.exports = [...AuthRoutes];