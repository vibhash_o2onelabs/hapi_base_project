
const Controller          =  require('../Controllers'),
      UniversalFunctions  =  require('../Utils/UniversalFunctions'),
      Joi                 =  require('joi'),
      Config              =  require('../Config');


let AuthRoutes=[

    {
        method: 'POST',
        path: '/api/user/',
        handler: function (request, reply) {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            if(userData && userData._id && userData.userType) {
                request.payload.userId = userData._id;
            }
            Controller.UserController.signup(request.payload,reply)
        },
        config: {
            description: 'register user',
          //  auth:'UserAuth',
            tags: ['api','user'],
            validate: {
                payload: {
                    first_name          : Joi.string().trim().required(),
                    last_name           : Joi.string().trim().required(),
                },
                failAction: UniversalFunctions.failActionFunction,
             //   headers    : UniversalFunctions.authorizationHeaderObj,

            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },




];

module.exports = [...AuthRoutes];