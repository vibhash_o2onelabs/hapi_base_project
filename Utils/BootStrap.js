 'use strict';

const DAOManager         =  require('../DAOManager').queries,
      Models             =  require('../Models'),
      Hash               =  require('./UniversalFunctions').hashPassword,
      Config             =  require('../Config'),
      UniversalFunction  =  require('./UniversalFunctions'),
      request            = require('request-promise');




class Bootstrap{
    static async bootStrapAdmin() {
        let admins=[
            {
                email: 'admin@fireFighter.com',
                password: Hash('321321'),
                name: 'Admin'
            },
            {
                email: 'test@fireFighter.com',
                password: Hash('321321'),
                name: 'Test_Admin'
            },
            {
                email: 'dev@fireFighter.com',
                password:Hash('321321'),
                name: 'Dev_Admin'
            }
        ];
        let promises=[],
            length=admins.length;

        for(let i=0;i<length;i++){

            let criteria={
                email:admins[i].email
            };

            let setQuery={
                $set:{
                    email:admins[i].email,
                    password:admins[i].password,
                    name:admins[i].name
                }
            };

            let options={
                new:true,
                upsert:true
            };

            promises.push(DAOManager.findAndUpdate(Models.Admins,criteria,setQuery,options));
        }

        return await Promise.all(promises);
    }
}

//Bootstrap.bootstrapCreditCriteria();
module.exports= Bootstrap;