const mongoose     =  require('mongoose'),
    Schema         =  mongoose.Schema,
    Config         =  require('../Config'),
    moment = require('moment-timezone'),
    time    = moment.tz("Asia/Baghdad");




const Newscategories = new Schema({
    created_by:{type: mongoose.Schema.ObjectId, ref: 'Editors'},
    category_name:{type:String,required:true,unique:true},
    category_photo:{type:String,sparse:true},
    is_deleted: { type: Boolean, required: true,default:false},
    created_on: { type: String, required:true,default:time.format()},
}, { versionKey: false });




module.exports = mongoose.model('Newscategories',Newscategories );