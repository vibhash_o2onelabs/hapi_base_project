
'use strict';

const Joi           =  require('joi'),
      MD5           =  require('md5'),
      Boom          =  require('boom'),
      CONFIG        =  require('../Config'),
      Randomstring  =  require('randomstring'),
      GeoPoint      =  require('geopoint'),
      HandleBars    =  require('handlebars'),
      moment        =  require('moment'),
      ObjectId      =  require('mongoose').Types.ObjectId,
      csv           =  require('csvtojson');


const sendError = function (data) {
    if (typeof data == 'object' && data.hasOwnProperty('statusCode') && data.hasOwnProperty('customMessage')) {
        console.log('attaching resposnetype',data.type);
        let msg=data.customMessage;
        msg= msg.replace(msg.charAt(0),msg.charAt(0).toUpperCase());
        let errorToSend = new Boom(msg,{statusCode:data.statusCode});
        errorToSend.output.payload.responseType = data.type;
        return errorToSend;
    } else {
        let errorToSend = '';
        if (typeof data == 'object') {
            if (data.name == 'MongoError') {
                errorToSend += CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DB_ERROR.customMessage;
                if (data.code = 11000) {
                    let duplicateValue = data.errmsg && data.errmsg.substr(data.errmsg.lastIndexOf('{ : "') + 5);
                    duplicateValue = duplicateValue.replace('}','');
                    errorToSend += CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE.customMessage + " : " + duplicateValue;
                    if (data.message.indexOf('customer_1_streetAddress_1_city_1_state_1_country_1_zip_1')>-1){
                        errorToSend = CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_ADDRESS.customMessage;
                    }
                }
            } else if (data.name == 'ApplicationError') {
                errorToSend += CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.APP_ERROR.customMessage + ' : ';
            } else if (data.name == 'ValidationError') {
                errorToSend += CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.APP_ERROR.customMessage + data.message;
            } else if (data.name == 'CastError') {
                errorToSend += CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DB_ERROR.customMessage + CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID.customMessage + data.value;
            }
        } else {
            errorToSend = data
        }
        let customErrorMessage = errorToSend;
        if (typeof customErrorMessage == 'string'){
            if (errorToSend.indexOf("[") > -1) {
                customErrorMessage = errorToSend.substr(errorToSend.indexOf("["));
            }
            customErrorMessage = customErrorMessage && customErrorMessage.replace(/"/g, '');
            customErrorMessage = customErrorMessage && customErrorMessage.replace('[', '');
            customErrorMessage = customErrorMessage && customErrorMessage.replace(']', '');

            customErrorMessage=customErrorMessage.replace(customErrorMessage.charAt(0),customErrorMessage.charAt(0).toUpperCase());
        }
        return new Boom (customErrorMessage,{statusCode:400});
    }
};

const sendSuccess = function (successMsg, data) {
    successMsg = successMsg || CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT.customMessage;
    if (typeof successMsg == 'object' && successMsg.hasOwnProperty('statusCode') && successMsg.hasOwnProperty('customMessage')) {
        return {statusCode:successMsg.statusCode, message: successMsg.customMessage, data: data || null};

    }else {
        return {statusCode:200, message: successMsg, data: data || null};

    }
};




const failActionFunction = function (request, reply, source, error) {

    let customErrorMessage = '';
    if (error.output.payload.message.indexOf("[") > -1) {
        customErrorMessage = error.output.payload.message.substr(error.output.payload.message.indexOf("["));
    } else {
        customErrorMessage = error.output.payload.message;
    }
    customErrorMessage = customErrorMessage.replace(/"/g, '');
    customErrorMessage = customErrorMessage.replace('[', '');
    customErrorMessage = customErrorMessage.replace(']', '');
    customErrorMessage=customErrorMessage.replace(customErrorMessage.charAt(0),customErrorMessage.charAt(0).toUpperCase());
    error.output.payload.message = customErrorMessage;
    delete error.output.payload.validation;
    return reply(error);
};


const customQueryDataValidations = function (type,key, data, callback) {
    let schema = {};
    switch(type){
        case 'PHONE_NO' : schema[key] = Joi.string().regex(/^[0-9]+$/).length(10);
            break;
        case 'NAME' : schema[key] = Joi.string().regex(/^[a-zA-Z ]+$/).min(2);
            break;
        case 'BOOLEAN' : schema[key] = Joi.boolean();
            break;
    }
    let value = {};
    value[key] = data;

    Joi.validate(value, schema, callback);
};


const authorizationHeaderObj = Joi.object({
    authorization: Joi.string().required()
}).unknown();




const CryptData = function (stringToCrypt) {
    return MD5(MD5(stringToCrypt));
};

const hashPassword=function(plainTextPassword){

    return MD5(MD5(plainTextPassword));
};

const compareHashPassword=function(plainTextPassword,hash){
    return MD5(MD5(plainTextPassword)) === hash;

};




module.exports = {
    sendError                             : sendError,
    sendSuccess                           : sendSuccess,
    CryptData                             : CryptData,
    failActionFunction                    : failActionFunction,

    customQueryDataValidations            : customQueryDataValidations,
    hashPassword                          : hashPassword,
    compareHashPassword                   : compareHashPassword,
    authorizationHeaderObj:authorizationHeaderObj,

};