
const AppConstants=require('./appConstants');

module.exports = {
    TEST:{
        USER:'fireFighterTest',
        PASS:'cKFpCAuZa6D5QXkh',
        DB_IP:'54.244.180.60',
        DB_NAME:'fireFighterTest',
        PORT:AppConstants.SERVER.PORTS.HAPI.TEST
    },
    DEV:{
        USER:'fireFighterDev',
        PASS:'cKFpCAuZa6D5QXkh',
        DB_IP:'54.244.180.60',
        DB_NAME:'fireFighterDev',
        PORT:AppConstants.SERVER.PORTS.HAPI.DEV
    },
    LIVE:{
        USER:'fireFighterLive',
        PASS:'cKFpCAuZa6D5QXkh',
        DB_IP:'54.244.180.60',
        DB_NAME:'fireFighterLive',
        PORT:AppConstants.SERVER.PORTS.HAPI.LIVE
    },
    DEFAULT:{
        USER:'',
        PASS:'',
        DB_IP:'localhost',
        DB_NAME:'HapiTest',
        PORT:AppConstants.SERVER.PORTS.HAPI.DEV
    }
};