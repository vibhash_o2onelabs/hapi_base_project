
'use strict';

let SERVER = {
    APP_NAME: 'NewsApp',
    PORTS: {
        HAPI:{
            LIVE : 3000,
            TEST : 3000,
            DEV  : 3000,
        }
    },
    TOKEN_EXPIRATION_IN_DAYS: '1d',
    JWT_SECRET_KEY: '1233',
    GOOGLE_API_KEY : '',
    COUNTRY_CODE : '+91',
    MAX_DISTANCE_RADIUS_TO_SEARCH : '1',
    THUMB_WIDTH : 200,
    THUMB_HEIGHT : 200,
    BASE_DELIVERY_FEE : 25,
    COST_PER_KM: 9, // In USD
    DOMAIN_NAME : 'http://localhost:8000/',
    SUPPORT_EMAIL : 'vibhash.tiwari@o2onelabs.com',
    FILE_PART_SIZE: 5242880                //  5 mb in bytes
};




let DATABASE = {
    PROJECT : '"fireFighter"',
    ONE_DAY_MILISECONDS:86400000,
    DEFAULT_LANGUAGE : 'English',
    //EMAIL_VERIFICATION_LINK:'http://192.168.102.102:8084/verified-mail/',   // local
    EMAIL_VERIFICATION_LINK:'http://dev.lukiwave.com:8084/verified-mail/',
    //PASSWORD_RESET_LINK:'http://192.168.102.102:8084/recover-password/',
    PASSWORD_RESET_LINK:'http://dev.lukiwave.com:8084/recover-password/',
    //SIGN_UP_LINK:'http://192.168.102.102:8084/sign-up/',
    SIGN_UP_LINK:'http://dev.lukiwave.com:8084/sign-up/',
    OTP_LENGTH:4,
    VENDOR_COMMISSION:0,
    VENDOR_PAYMENT_CYCLE:7,
    ADMIN_EMAIL:'lovepreet@code-brew.com',
    COUNTRY_CODE:'+91',
    DEFAULT_RADIUS:100,

    DEVICE_TYPES: {
        IOS: 'IOS',
        ANDROID: 'ANDROID'
    },
    GENDER: {
        MALE:'MALE',
        FEMALE:'FEMALE'
    },
    DEFAULT_IMAGE:{
        PRODUCT_IMAGE_URL:"http://www.gamedevhelper.com/images/default_product_icon.png",
        USER_IMAGE_URL:"http://cshark.it/img/default_profile.jpg",
        DEFAULT:{
            original:"https://www.femina.in/images/default-user.png",
            thumbnail:"https://www.femina.in/images/default-user.png"
        },
        DEFAULT_GROUP : {
            original:"https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-group-256.png",
            thumbnail:"https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-group-256.png"
        },
        COUNTRY_IMAGE:{
            UK:{
                original:"http://freestock.ca/uk_grunge_flag__inverted_sjpg1956.jpg",
                thumbnail:"http://freestock.ca/uk_grunge_flag__inverted_sjpg1956.jpg"
            },
            INDIA:{
                original:"https://thediplomat.com/wp-content/uploads/2013/11/Flag_of_India-386x257.png",
                thumbnail:"https://thediplomat.com/wp-content/uploads/2013/11/Flag_of_India-386x257.png"
            }
        }
    },
    USER_TYPE:{
        REGISTERED:'REGISTERED',
        GUEST:'GUEST'
    },
    TOKEN_FIELDS:{
        ACCESS_TOKEN:'accessToken'
    },
    USER_ROLES:{
      USERS:'USERS',
      EDITORS:'EDITORS',
      ADMIN:'ADMIN'
    },


};

let SOCKET_EVENTS = {
    START : {
        CONNECTION : 'connection',
        DISCONNECT : 'disconnect',
        SOCKET_CONNECTED : 'socketConnected'
    },
    CHAT : {
        SEND_MESSAGE : 'sendMessage',
        RECEIVE_MESSAGE : 'receiveMessage',
        READ_MESSAGE : 'readMessage'
    },
    ERROR : {
        PARAMETER_ERROR : 'parameterError',
        SOCKET_ERROR    : 'socketError'
    }
};

let STATUS_MSG = {
    ERROR: {
        SOCKET_ERRORS : {
            MESSAGE_FOR_REQUIRED : {
                statusCode:400,
                type: 'MESSAGE_FOR_REQUIRED',
                customMessage : 'Message for is required. It can be only Group or Single.'
            },
            MESSAGE_TYPE_REQUIRED : {
                statusCode:400,
                type: 'MESSAGE_TYPE_REQUIRED',
                customMessage : 'Message type is required. It can be only Text or File.'
            },
            INVALID_TYPE_OF_ID : {
                statusCode:400,
                type: 'INVALID_TYPE_OF_ID',
                customMessage : 'Invalid type of sender or reciever id.'
            },
            RECEIVER_REQUIRED : {
                statusCode:400,
                type: 'RECEIVER_REQUIRED',
                customMessage : 'Receiver id is required.'
            },
            INVALID_USER_PASS: {
                statusCode:400,
                customMessage : 'Incorrect email or password',
                type : 'INVALID_USER_PASS'
            },
            SENDER_REQUIRED : {
                statusCode:400,
                type: 'SENDER_REQUIRED',
                customMessage : 'Sender id is required.'
            },
            MESSAGE_ID_REQUIRED : {
                statusCode:400,
                type: 'MESSAGE_ID_REQUIRED',
                customMessage : 'Message id is required.'
            },
            INVALID_MESSAGE_ID : {
                statusCode:400,
                type: 'INVALID_MESSAGE_ID',
                customMessage : 'Message id is invalid.'
            },
            MESSAGE_REQUIRED : {
                statusCode:400,
                type: 'MESSAGE_REQUIRED',
                customMessage : 'Message required in case of of text message.'
            },
            FILE_REQUIRED : {
                statusCode:400,
                type: 'FILE_REQUIRED',
                customMessage : 'File required in case of of file type of message.'
            },
            SOCKET_ERROR : {
                statusCode: 400,
                customMessage: 'Error in connecting to socket',
                type: 'SOCKET_ERROR'
            },
            IMP_ERROR : {
                statusCode: 500,
                customMessage: 'Something went wrong in socket implimentation.',
                type: 'IMP_ERROR'
            }
        },
        INVALID_LOCATIONS_IDS: {
            statusCode:400,
            type: 'INVALID_LOCATIONS_IDS',
            customMessage : 'Invalid locations ids. '
        },
        OLD_PASSWORD_NOT_MATCH:{
            statusCode:400,
            type: 'OLD_PASSWORD_NOT_MATCH',
            customMessage : 'Old Password Not Match . '
        },
        ATLEAST_ONE_PARAMETER_REQUIRED: {
            statusCode:400,
            type: 'ATLEAST_ONE_PARAMETER_REQUIRED',
            customMessage : 'For getting single chat data pass sender and reciver id and for group chat data pass group id. '
        },
        SOME_LOCATION_IDS_INVALID: {
            statusCode:400,
            type: 'SOME_LOCATION_IDS_INVALID',
            customMessage : 'Some location ids are invalid. '
        },
        SOMETHING_WENT_WRONG: {
            statusCode:400,
            type: 'SOMETHING_WENT_WRONG',
            customMessage : 'Something went wrong on server. '
        },
        SOME_USER_IDS_INVALID: {
            statusCode:400,
            type: 'SOME_USER_IDS_INVALID',
            customMessage : 'Some user ids are invalid. '
        },
        INVALID_FIELDS_CONTAINED: {
            statusCode:400,
            type: 'INVALID_FIELDS_CONTAINED',
            customMessage : 'File Contained invalid fields please varify. '
        },
        FILE_NOT_RECEIVED: {
            statusCode:400,
            type: 'FILE_NOT_RECEIVED',
            customMessage : 'File is not received.'
        },
        FILE_FORMAT_MISMATCH: {
            statusCode:400,
            type: 'FILE_FORMAT_MISMATCH',
            customMessage : 'Sorry file format is not matched. '
        },
        FILE_NAME_OR_PATH_MISSING: {
            statusCode:400,
            type: 'FILE_NAME_OR_PATH_MISSING',
            customMessage : 'Sorry filename or path is missing. '
        },
        FILE_INVALID_FIELDS: {
            statusCode:400,
            type: 'FILE_INVALID_FIELDS',
            customMessage : 'Sorry file contain invalid fields. '
        },
        EMPTY_FILE: {
            statusCode:400,
            type: 'EMPTY_FILE',
            customMessage : 'Sorry file is empty. '
        },
        STORE_ALREADY_EXIST: {
            statusCode:401,
            type: 'STORE_ALREADY_EXIST',
            customMessage : 'Store Already Exist.'
        },
        ALREADY_EXIST: {
            statusCode:401,
            type: 'ALREADY_EXIST',
            customMessage : 'Already Exist '
        },
        DATA_NOT_FOUND: {
            statusCode:401,
            type: 'DATA_NOT_FOUND',
            customMessage : 'empty data'
        },
        INVALID_USER_PASS: {
            statusCode:401,
            type: 'INVALID_USER_PASS',
            customMessage : 'Invalid username or password'
        },
        INVALID_USER_TYPE: {
            statusCode:401,
            type: 'INVALID_USER_TYPE',
            customMessage : 'Unauthorized user.'
        },
         INVALID_PASSWORD: {
            statusCode:400,
            type: 'INVALID_PASSWORD',
            customMessage : 'Invalid password'
        },

        RESPONSE_TIME_OVER: {
            statusCode:405,
            type: 'RESPONSE_TIME_OVER',
            customMessage : 'Your response time for this order is over. '
        },
        INVALID_OLD_PASSWORD: {
            statusCode:400,
            type: 'INVALID_OLD_PASSWORD',
            customMessage : 'Invalid Old password.'
        },
        INVALID_DEAL_ID: {
            statusCode:400,
            type: 'INVALID_DEAL_ID',
            customMessage : 'invalid Deal id.'
        },
        INVALID_SELECTION: {
            statusCode:400,
            type: 'INVALID_SELECTION',
            customMessage : 'Invalid Selection.'
        },
        INVALID_SOCIAL_ACCOUNT: {
            statusCode:400,
            type: 'INVALID_SOCIAL_ACCOUNT',
            customMessage : 'Invalid social account.'
        },
        INVALID_ORDER_ID: {
            statusCode:400,
            type: 'INVALID_ORDER_ID',
            customMessage : 'invalid orderId.'
        },
        INVALID_DATE_RANGE: {
            statusCode:400,
            type: 'INVALID_DATE_RANGE',
            customMessage : 'invalid date range provided.'
        },
        ID_REQUIRED: {
            statusCode:400,
            type: 'ID REQUIRED',
            customMessage : 'Id is required .'
        },
        REF_ID_REQUIRED: {
            statusCode:400,
            type: 'REF_ID_REQUIRED',
            customMessage : 'Reference id is required while adding or upadting country,state,city .'
        },
        INVALID_TOTAL_TICKETS: {
            statusCode:400,
            type: 'INVALID_TOTAL_TICKETS',
            customMessage : 'Total tickets can not less than already sold tickets.'
        },
        SPONSOR_DATA_INSUFFICIENT: {
            statusCode:400,
            type: 'SPONSOR_DATA_INSUFFICIENT',
            customMessage : 'Sponsor image , name and url is required.'
        },
        CASH_MONEY_ERROR: {
            statusCode:400,
            type: 'CASH_MONEY_ERROR',
            customMessage : 'Sorry cash prize can not have any subCategory.'
        },
        NAME_FORMAT_INVALID:{
            statusCode:400,
            type: 'NAME_FORMAT_INVALID',
            customMessage : 'First name and Last name can only contain alphabets.'
        },
        SOCIAL_EMAIL_REQUIRED:{
            statusCode:400,
            type: 'SOCIAL_EMAIL_REQUIRED',
            customMessage : 'Social email is required.'
        },
        SOCIAL_KEY_REQUIRED:{
            statusCode:400,
            type: 'SOCIAL_KEY_REQUIRED',
            customMessage : 'Social key is required.'
        },

        COMPANY_NAME_FORMAT_INVALID:{
            statusCode:400,
            type: 'COMPANY_NAME_FORMAT_INVALID',
            customMessage : 'Company name can only contain alphabets or a white space inbetween.'
        },
        EMPLOYER_NAME_FORMAT_INVALID:{
            statusCode:400,
            type: 'EMPLOYER_NAME_FORMAT_INVALID',
            customMessage : 'Employer name can only contain alphabets or a white space inbetween.'
        },
        LAT_LONG_REQUIRED:{
            statusCode:400,
            type: 'LAT_LONG_REQUIRED',
            customMessage : 'For edit send both lat and long of company.'
        },
        START_AND_END_TIME_REQUIRED:{
            statusCode:400,
            type: 'START_AND_END_TIME_REQUIRED',
            customMessage : 'start and end time are required.'
        },
        SERVICE_CAT_ID_REQUIRED:{
            statusCode:400,
            type: 'SERVICE_CAT_ID_REQUIRED',
            customMessage : 'Service categoryId is required.'
        },
        STATE_ID_REQUIRED:{
            statusCode:400,
            type: 'STATE_ID_REQUIRED',
            customMessage : 'State id is required.'
        },
        VIDEO_DURATION_ERROR:{
            statusCode:400,
            type: 'VIDEO_DURATION_ERROR',
            customMessage : 'video should be atleast of 6 seconds.'
        },
        STATE_REQUIRED:{
            statusCode:400,
            type: 'STATE_REQUIRED',
            customMessage : 'State is required.'
        },
        INVESTMENT_ID_REQUIRED:{
            statusCode:400,
            type: 'INVESTMENT_ID_REQUIRED',
            customMessage : 'Investment id is required.'
        },
        INVESTMENT_TYPE_ID_REQUIRED:{
            statusCode:400,
            type: 'INVESTMENT_TYPE_ID_REQUIRED',
            customMessage : 'Investment type  id is required.'
        },
        PROOF_ID_REQUIRED:{
            statusCode:400,
            type: 'PROOF_ID_REQUIRED',
            customMessage : 'Proof id is required.'
        },
        SSNTAX_ID_REQUIRED:{
            statusCode:400,
            type: 'SSNTAX_ID_REQUIRED',
            customMessage : 'SSNTAX id is required.'
        },
        DOB_REQUIRED:{
            statusCode:400,
            type: 'DOB_REQUIRED',
            customMessage : 'DOB is required.'
        },
        EMPLOYER_NAME_REQUIRED:{
            statusCode:400,
            type: 'EMPLOYER_NAME_REQUIRED',
            customMessage : 'Employer name is required.'
        },
        OCCUPATION_REQUIRED:{
            statusCode:400,
            type: 'OCCUPATION_REQUIRED',
            customMessage : 'Occupation is required.'
        },
        INVALID_BUSINESS_ID:{
            statusCode:400,
            type: 'INVALID_BUSINESS_ID',
            customMessage : 'Please provide valid business id.'
        },
        INVALID_OPERATION: {
            statusCode:400,
            type: 'INVALID_OPERATION',
            customMessage : 'invalid operation please verify.'
        },
        INVALID_PRODUCT: {
            statusCode:400,
            type: 'INVALID_PRODUCT',
            customMessage : '{{productId}} is invalid product id.'
        },
        INVALID_VENDOR: {
            statusCode:400,
            type: 'INVALID_VENDOR',
            customMessage : '{{vendorId}} is invalid vendor.'
        },
        MINIMUM_ORDER_NOT_BEAT: {
            statusCode:400,
            type: 'MINIMUM_ORDER_NOT_BEAT',
            customMessage : 'minimum order is not fulfilled of this vendor.'
        },
        TOKEN_ALREADY_EXPIRED: {
            statusCode:401,
            customMessage : 'Token Already Expired',
            type : 'TOKEN_ALREADY_EXPIRED'
        },
        AUTHORIZATION_REQUIRED: {
            statusCode:400,
            customMessage : 'Authorization is required.',
            type : 'AUTHORIZATION_REQUIRED'
        },
        DB_ERROR: {
            statusCode:400,
            customMessage : 'DB Error : ',
            type : 'DB_ERROR'
        },
        INVALID_ID: {
            statusCode:400,
            customMessage : 'Invalid id provided.',
            type : 'INVALID_ID'
        },
        INVALID_TYPE__ID: {
            statusCode:400,
            customMessage : 'Invalid id provided.',
            type : 'INVALID_ID'
        },
        INVALID_IDS: {
            statusCode:400,
            customMessage : 'Invalid ids provided.',
            type : 'INVALID_IDS'
        },
        INVALID_REF_ID: {
            statusCode:400,
            customMessage : 'Invalid reference id provided.',
            type : 'INVALID_REF_ID'
        },
        ONE_FILE_COMPL: {
            statusCode:400,
            customMessage : 'Sorry one file is compulsory.',
            type : 'ONE_FILE_COMPL'
        },
        ACCOUNT_BLOCKED: {
            statusCode:400,
            customMessage : 'Sorry your account is blocked by admin.',
            type : 'ACCOUNT_BLOCKED'
        },
        USERS_ARE_NOT_FRIEND: {
            statusCode:400,
            customMessage : 'Sorry some users are not friends of user.',
            type : 'USERS_ARE_NOT_FRIEND'
        },
        PUSH_TYPE_MISMATCH: {
            statusCode:400,
            customMessage : 'Sorry no push type is mathced.',
            type : 'PUSH_TYPE_MISMATCH'
        },
        EITHER_LOCATION_OR_COUNTRY: {
            statusCode:400,
            customMessage : 'Either location or country need to be selected.',
            type : 'EITHER_LOCATION_OR_COUNTRY'
        },
        LOTTERY_CAN_NOT_EDITED: {
            statusCode:400,
            customMessage : 'Sorry lottery can be edited in only save mode.',
            type : 'LOTTERY_CAN_NOT_EDITED'
        },
        INVALID_INVESTMENT_ID: {
            statusCode:400,
            customMessage : 'Invalid investment id provided.',
            type : 'INVALID_INVESTMENT_ID'
        },
        INVALID_PROOF_ID: {
            statusCode:400,
            customMessage : 'Invalid Proof id provided.',
            type : 'INVALID_PROOF_ID'
        },
        INVALID_FILE_ID: {
            statusCode:400,
            customMessage : 'Invalid file id provided.',
            type : 'INVALID_FILE_ID'
        },
        INVALID_FILE_EXT: {
            statusCode:400,
            customMessage : 'Invalid file extention.',
            type : 'INVALID_FILE_ID'
        },
        INVALID_BUSINESS_TYPE: {
            statusCode:400,
            customMessage : 'Invalid business id provided..',
            type : 'INVALID_BUSINESS_TYPE'
        },
        INVALID_USER_ID: {
            statusCode:400,
            customMessage : 'Invalid User Id ',
            type : 'INVALID_USER_ID'
        },
        INVALID_VENDOR_ID: {
            statusCode:400,
            customMessage : 'Invalid Vendor Id : ',
            type : ' INVALID_VENDOR_ID'
        },
        INVALID_GOODIE_ID: {
            statusCode:400,
            customMessage : 'Invalid Goodie Id : ',
            type : ' INVALID_GOODIE_ID'
        },
        INVALID_DELIVERY_ADDRESS_ID: {
            statusCode:400,
            customMessage : 'Invalid Delivery Address Id . ',
            type : ' INVALID_DELIVERY_ADDRESS_ID'
        },
        INVALID_CARD_ID: {
            statusCode:400,
            customMessage : 'Invalid Card Id. ',
            type : ' INVALID_CARD_ID'
        },
        INVALID_COUNTRY_ID: {
            statusCode:400,
            customMessage : 'Invalid Country Id. ',
            type : ' INVALID_COUNTRY_ID'
        },
        INVALID_GROUP_ID: {
            statusCode:400,
            customMessage : 'Invalid group id. ',
            type : ' INVALID_GROUP_ID'
        },
        INVALID_SENDER_ID: {
            statusCode:400,
            customMessage : 'Invalid sender id. ',
            type : ' INVALID_SENDER_ID'
        },
        INVALID_RECEIVER_ID: {
            statusCode:400,
            customMessage : 'Invalid receiver id. ',
            type : ' INVALID_RECEIVER_ID'
        },
        INVALID_SERVICEMAN_ID: {
            statusCode:400,
            customMessage : 'Invalid Serviceman Id.',
            type : ' INVALID_SERVICEMAN_ID'
        },
        INVALID_SUPPLIER_ID: {
            statusCode:400,
            customMessage : 'Invalid Supplier Id.',
            type : ' INVALID_SUPPLIER_ID'
        },
        INVALID_INVESTOR_ID: {
            statusCode:400,
            customMessage : 'Invalid Investor Id.',
            type : ' INVALID_INVESTOR_ID'
        },
        INVALID_CITY_ID: {
            statusCode:400,
            customMessage : 'Invalid City Id .',
            type : ' INVALID_CITY_ID'
        },
        ALREADY_APPROOVED: {
            statusCode:400,
            customMessage : 'Already Approved. ',
            type : 'ALREADY_APPROOVED'
        },
        INSUFFIECIENT_DATA: {
            statusCode:400,
            customMessage : 'Insifficient data. ',
            type : 'INSUFFIECIENT_DATA'
        },
        ATLEAST_ONE_REQUIRED: {
            statusCode:400,
            customMessage : 'Atleast one setting is required to update.. ',
            type : 'ATLEAST_ONE_REQUIRED'
        },
        ALREADY_CONNECTED: {
            statusCode:400,
            customMessage : 'Account is alredy connected with other account . ',
            type : 'ALREADY_CONNECTED'
        },
        ALREADY_REJECTED: {
            statusCode:400,
            customMessage : 'Already Rejected. ',
            type : 'ALREADY_REJECTED'
        },
        ALREADY_DELIVERED: {
            statusCode:400,
            customMessage : 'Already Status is Delivered.',
            type : 'ALREADY_DELIVERED'
        },
        ALREADY_CANCELED: {
            statusCode:400,
            customMessage : 'Already Status is Canceled. ',
            type : 'ALREADY_CANCELED'
        },
        ALREADY_MARK_PLACED: {
            statusCode:400,
            customMessage : 'Already Status is Mark As Placed . ',
            type : 'ALREADY_MARK_PLACED'
        },
        ALREADY_BLOCKED: {
            statusCode:400,
            customMessage : 'Already Blocked : ',
            type : 'ALREADY_BLOCKED'
        },
        ALREADY_UNBLOCKED: {
            statusCode:400,
            customMessage : 'Already UnBlocked : ',
            type : 'ALREADY_UNBLOCKED'
        },
        APP_ERROR: {
            statusCode:400,
            customMessage : 'Application Error',
            type : 'APP_ERROR'
        },
        ADDRESS_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Address not found',
            type : 'ADDRESS_NOT_FOUND'
        },
        COUPEN_TYPE_CHANGE_NOT_ALLOW: {
            statusCode:400,
            customMessage : 'Coupen type can not change',
            type : 'COUPEN_TYPE_CHANGE_NOT_ALLOW'
        },
        SAME_ADDRESS_ID: {
            statusCode:400,
            customMessage : 'Pickup and Delivery Address Cannot Be Same',
            type : 'SAME_ADDRESS_ID'
        },
        PICKUP_ADDRESS_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Pickup Address not found',
            type : 'PICKUP_ADDRESS_NOT_FOUND'
        },
        DELIVERY_ADDRESS_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Delivery Address not found',
            type : 'DELIVERY_ADDRESS_NOT_FOUND'
        },
        IMP_ERROR: {
            statusCode:500,
            customMessage : 'Implementation Error',
            type : 'IMP_ERROR'
        },
        INVALID_FILE: {
            statusCode:400,
            customMessage : 'Invalid file only .mp4,.3gp,.avi  and .jpg,.png,.jpeg and .csv,.pdf,.xls,.doc,.docx files are accepted.',
            type : 'INVALID_FILE'
        },
        INVALID_IMAGE: {
            statusCode:400,
            customMessage : 'Image can only .jpg,.png,.jpeg .',
            type : 'INVALID_FILE'
        },
        INVALID_FILE_PROOF: {
            statusCode:400,
            customMessage : 'Invalid file only  .jpg,.png,.jpeg and .pdf files are accepted.',
            type : 'INVALID_FILE'
        },
        APP_VERSION_ERROR: {
            statusCode:400,
            customMessage : 'One of the latest version or updated version value must be present',
            type : 'APP_VERSION_ERROR'
        },
        INVALID_TOKEN: {
            statusCode:401,
            customMessage : 'Your session has been expired.',
            type : 'INVALID_TOKEN'
        },
        INVALID_VERIFICATION_TOKEN: {
            statusCode:400,
            customMessage : 'Invalid verification token is provided.',
            type : 'INVALID_VERIFICATION_TOKEN'
        },
        INVALID_CODE: {
            statusCode:400,
            customMessage : 'Invalid Verification Code',
            type : 'INVALID_CODE'
        },
        INVALID_EMAIL: {
            statusCode:400,
            customMessage : 'Invalid Email',
            type : 'INVALID_EMAIL'
        },
        DEFAULT: {
            statusCode:400,
            customMessage : 'Error',
            type : 'DEFAULT'
        },
        PHONE_NO_EXIST: {
            statusCode:400,
            customMessage : 'Phone No Already Exist',
            type : 'PHONE_NO_EXIST'
        },
        SKU_EXIST: {
            statusCode:400,
            customMessage : 'SKU No Already Exist',
            type : 'SKU_EXIST'
        },
        ORDER_EXIST: {
            statusCode:400,
            customMessage : 'Order No Already Exist',
            type : 'ORDER_EXIST'
        },
        CATEGORY_EXIST: {
            statusCode:400,
            customMessage : 'Category Already Exist',
            type : 'CATEGORY_EXIST'
        },
         PRODUCT_EXIST: {
            statusCode:400,
            customMessage : 'Product Already Existed',
            type : 'PRODUCT_EXIST'
        },
        GOODIE_EXIST: {
            statusCode:400,
            customMessage : 'Goodie Already Existed',
            type : 'GOODIE_EXIST'
        },
        DUPLICATE_BARCODE: {
            statusCode:400,
            customMessage : 'Bar Code Exist.',
            type : 'DUPLICATE_BARCODE'
        },
        INVALID_PRODUCT_ID:{
            statusCode:400,
            customMessage : 'Invalid Product Id',
            type : 'INVALID_PRODUCT_ID'
        },
        INVALID_LOCATION_ID:{
            statusCode:400,
            customMessage : 'Invalid Location Id',
            type : 'INVALID_LOCATION_ID'
        },
        INVALID_SERVICE_CAT_ID:{
            statusCode:400,
            customMessage : 'Invalid service category id.',
            type : 'INVALID_SERVICE_CAT_ID'
        },
        INVALID_STATE_ID:{
            statusCode:400,
            customMessage : 'Invalid stateId.',
            type : 'INVALID_STATE_ID'
        },
        INVALID_CATEGORY_ID:{
            statusCode:400,
            customMessage : 'Invalid Category Id',
            type : 'INVALID_CATEGORY_ID'
        },
         INVALID_CATEGORY_ID_TYPE:{
            statusCode:400,
            customMessage : 'Invalid Category Id Type',
            type : 'INVALID_CATEGORY_ID_TYPE'
        },
        INVALID_SUB_CATEGORY_ID:{
            statusCode:400,
            customMessage : 'Invalid SubCategory Id',
            type : 'INVALID_SUB_CATEGORY_ID'
        },
        INVALID_SUB_CATEGORY_ID_TYPE:{
            statusCode:400,
            customMessage : 'Invalid SubCategory Id Type',
            type : 'INVALID_SUB_CATEGORY_ID_TYPE'
        },
        EMAIL_EXIST: {
            statusCode:400,
            customMessage : 'Email Already Exist',
            type : 'EMAIL_EXIST'
        },
        INVALID_USER_CREDENTIALS: {
            statusCode:400,
            customMessage : 'Invalid User Credentials',
            type : 'INVALID_USER_CREDENTIALS'
        },
        DUPLICATE: {
            statusCode:400,
            customMessage : 'Duplicate Entry',
            type : 'DUPLICATE'
        },
        DUPLICATE_LOCATION: {
            statusCode:400,
            customMessage : 'This location is already exist.',
            type : 'DUPLICATE_LOCATION'
        },
        DUPLICATE_ADDRESS: {
            statusCode:400,
            customMessage : 'Address Already Exist',
            type : 'DUPLICATE_ADDRESS'
        },
        UNIQUE_CODE_LIMIT_REACHED: {
            statusCode:400,
            customMessage : 'Cannot Generate Unique Code, All combinations are used',
            type : 'UNIQUE_CODE_LIMIT_REACHED'
        },
        INVALID_REFERRAL_CODE: {
            statusCode:400,
            customMessage : 'Invalid Referral Code',
            type : 'INVALID_REFERRAL_CODE'
        },
        FACEBOOK_ID_PASSWORD_ERROR: {
            statusCode:400,
            customMessage : 'Only one field should be filled at a time, either facebookId or password',
            type : 'FACEBOOK_ID_PASSWORD_ERROR'
        },
         KEY_REQUIRED:{
            statusCode:400,
            customMessage : 'Facebook Or Google Key is required',
            type : 'KEY_REQUIRED'
        },
         PRICE_REQUIRED:{
            statusCode:400,
            customMessage : 'Price Required',
            type : 'PRICE_REQUIRED'
        },
        SUBCATEGORY_REQUIRED:{
            statusCode:400,
            customMessage : 'Subcategory required if prize is not a cash one. ',
            type : 'SUBCATEGORY_REQUIRED'
        },
        LOCATIONS_REQUIRED_IN_LOCAL:{
            statusCode:400,
            customMessage : 'Locations details are required in case of Local lottery type. ',
            type : 'LOCATIONS_REQUIRED_IN_LOCAL'
        },
        NAME_REQUIRED:{
            statusCode:400,
            customMessage : 'Name is required',
            type : 'NAME_REQUIRED'
        },
        PASSWORD_REQUIRED: {
            statusCode:400,
            customMessage : 'Password is required',
            type : 'PASSWORD_REQUIRED'
        },
        OPTION_REQUIRED: {
            statusCode:400,
            customMessage : 'Option is required',
            type : 'OPTION_REQUIRED'
        },
        CATEGORY_ID_REQUIRED:{
            statusCode:400,
            customMessage : 'Category Id is Required',
            type : 'CATEGORY_ID_REQUIRED'
        },
        DELIVERY_ADDRESS_REQUIRED:{
            statusCode:400,
            customMessage : 'Delivery Address is Required.',
            type : 'DELIVERY_ADDRESS_REQUIRED'
        },
        DEVICE_TOKEN_REQUIRED:{
            statusCode:400,
            customMessage : 'Device Token is Required.',
            type : 'DEVICETOKEN_REQUIRED'
        },
         IMAGE_REQUIRED:{
            statusCode:400,
            customMessage : 'Image Required',
            type : 'IMAGE_REQUIRED'
        },
        FILE_IS_REQUIRED:{
            statusCode:400,
            customMessage : 'File is required.',
            type : 'FILE_IS_REQUIRED'
        },
        FILES_ARE_REQUIRED:{
            statusCode:400,
            customMessage : 'Files are required.',
            type : 'FILES_ARE_REQUIRED'
        },
        PROOF_FILE_REQUIRED:{
            statusCode:400,
            customMessage : 'Proof file is required.',
            type : 'PROOF_FILE_REQUIRED'
        },
         EMAIL_REQUIRED:{
            statusCode:400,
            customMessage : 'Email is a required.',
            type : 'EMAIL_REQUIRED'
        },
        INVALID_COUPON: {
            statusCode:400,
            customMessage : 'Invalid Coupon',
            type : 'INVALID_COUPON'
        },
        INVALID_COUNTRY_CODE: {
            statusCode:400,
            customMessage : 'Invalid Country Code, Should be in the format +52',
            type : 'INVALID_COUNTRY_CODE'
        },
        INVALID_PHONE_NO_FORMAT: {
            statusCode:400,
            customMessage : 'Phone no. cannot start with 0',
            type : 'INVALID_PHONE_NO_FORMAT'
        },
        INVALID_DELIVERY_LOCATION:{
            statusCode:400,
            customMessage : '',
            type : 'INVALID_DELIVERY_LOCATION'
        },
        COUNTRY_CODE_MISSING: {
            statusCode:400,
            customMessage : 'You forgot to enter the country code',
            type : 'COUNTRY_CODE_MISSING'
        },
        INVALID_PHONE_NO: {
            statusCode:400,
            customMessage : 'Invalid phone no.',
            type : 'INVALID_PHONE_NO'
        },
        PHONE_NO_MISSING: {
            statusCode:400,
            customMessage : 'You forgot to enter the phone no.',
            type : 'PHONE_NO_MISSING'
        },
        NOTHING_TO_UPDATE: {
            statusCode:400,
            customMessage : 'Nothing to update',
            type : 'NOTHING_TO_UPDATE'
        },
        NOT_FOUND: {
            statusCode:400,
            customMessage : 'User Not Found',
            type : 'NOT_FOUND'
        },
        OTP_NOT_VERIFIED: {
            statusCode:400,
            customMessage : 'Your otp verification is pending.',
            type : 'OTP_NOT_VERIFIED'
        },
        INVALID_RESET_PASSWORD_TOKEN: {
            statusCode:400,
            customMessage : 'Invalid Reset Password Token',
            type : 'INVALID_RESET_PASSWORD_TOKEN'
        },
        INCORRECT_PASSWORD: {
            statusCode:401,
            customMessage : 'Incorrect Password',
            type : 'INCORRECT_PASSWORD'
        },
        EMPTY_VALUE: {
            statusCode:400,
            customMessage : 'Empty String Not Allowed',
            type : 'EMPTY_VALUE'
        },
        PHONE_NOT_MATCH: {
            statusCode:400,
            customMessage : "Phone No. Doesn't Match",
            type : 'PHONE_NOT_MATCH'
        },
        SAME_PASSWORD: {
            statusCode:400,
            customMessage : 'Old password and new password are same',
            type : 'SAME_PASSWORD'
        },
        ACTIVE_PREVIOUS_SESSIONS: {
            statusCode:400,
            customMessage : 'You already have previous active sessions, confirm for flush',
            type : 'ACTIVE_PREVIOUS_SESSIONS'
        },
        EMAIL_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'Email Address Already Exists',
            type : 'EMAIL_ALREADY_EXIST'
        },
        PARENT_EMAIL_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'Account is already exist with "{{email}}" that is already connected with other account.',
            type : 'PARENT_EMAIL_ALREADY_EXIST'
        },
        PHONE_NO_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'Phone no Already Exists',
            type : 'PHONE_NO_ALREADY_EXIST'
        },
        ERROR_PROFILE_PIC_UPLOAD: {
            statusCode:400,
            customMessage : 'Profile pic is not a valid file',
            type : 'ERROR_PROFILE_PIC_UPLOAD'
        },
        PHONE_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'Phone no already exists',
            type : 'PHONE_ALREADY_EXIST'
        },
        EMAIL_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Email Not Found',
            type : 'EMAIL_NOT_FOUND'
        },
        CONTACT_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Invalid contact details given.',
            type : 'CONTACT_NOT_FOUND'
        },
        BLOCK_USER: {
            statusCode:400,
            customMessage : 'You are blocked by Admin',
            type : 'BLOCK_USER'
        },
        FACEBOOK_ID_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Facebook Id Not Found',
            type : 'FACEBOOK_ID_NOT_FOUND'
        },
        PHONE_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Phone No. Not Found',
            type : 'PHONE_NOT_FOUND'
        },
        INCORRECT_OLD_PASS: {
            statusCode:400,
            customMessage : 'Incorrect Old Password',
            type : 'INCORRECT_OLD_PASS'
        },
        UNAUTHORIZED: {
            statusCode:401,
            customMessage : 'You are not authorized to perform this action',
            type : 'UNAUTHORIZED'
        },
        POINTS_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'Points are already exist.',
            type : 'POINTS_ALREADY_EXIST'
        },
        GMAIL_EMAIL_EXIST:{
            statusCode:400,
            customMessage : 'You have already registered through gmail for same email. Please try login through gmail',
            type : 'GMAIL_EMAIL_EXIST'
        },
        fb_EMAIL_EXIST:{
            statusCode:400,
            customMessage : 'You have already registered through facebook for same email. Please try login through facebook',
            type : 'fb_EMAIL_EXIST'

        },
        USER_NOT_RESGISTER:{
            statusCode:400,
            customMessage : 'User Is Not Register With Us',
            type : 'USER_NOT_RESGISTER'
        },
        BLOCK_BY_ADMIN:{
            statusCode:400,
            customMessage : 'Block By Admin',
            type : 'BLOCK_BY_ADMIN'
        }

    },
    SUCCESS: {
        CREATED: {
            statusCode:201,
            customMessage : 'Created Successfully',
            type : 'CREATED'
        },
        DEFAULT: {
            statusCode:200,
            customMessage : 'Success',
            type : 'DEFAULT'
        },
        UPDATED: {
            statusCode:200,
            customMessage : 'Updated Successfully',
            type : 'UPDATED'
        },
        LOGIN:{
            statusCode:200,
            customMessage : 'Log In Successfully',
            type : 'LOGIN'
        },
        LOGOUT: {
            statusCode:200,
            customMessage : 'Logged Out Successfully',
            type : 'LOGOUT'
        },
        DELETED: {
            statusCode:200,
            customMessage : 'Deleted Successfully',
            type : 'DELETED'
        }
    }
};







let HYDRANT_STATUS={
        WORKING     :'WORKING',
        NOT_WORKING :'NOT_WORKING',
        DOUBTFUL    :'DOUBTFUL'
}

let BLOCK_USER = {
    BLOCK   :'BLOCK',
    UNBLOCK : 'UNBLOCK'
};
let swaggerDefaultResponseMessages = [
    {code: 200, message: 'OK'},
    {code: 400, message: 'Bad Request'},
    {code: 401, message: 'Unauthorized'},
    {code: 404, message: 'Data Not Found'},
    {code: 500, message: 'Internal Server Error'}
];


let NOTIFICATION_TYPE = {
    REGISTRATION_MAIL : 'Registration Mail',
    FORGOT_PASSWORD   : 'Forgot Password',
    FRIEND_REQUEST    : 'Friend Request',
    FRIEND_REQUEST_ACCEPTED : 'Friend Request Accepted',
    GROUP_CREATED     : 'Group Created',
    INVITATION        : 'Invitation'
};

let NOTIFICATION_CATEGORY = {
    FRIEND  : 'Friend',
    GENERAL : 'General',
    Message : 'Message'
};


let STATUS_CODE={
    ERROR_500:500,
    SUCCESS_200:200,
    ERROR_404:400,
}



let APP_CONSTANTS = {
    SERVER: SERVER,
    DATABASE: DATABASE,
    STATUS_MSG: STATUS_MSG,

    NOTIFICATION_TYPE:NOTIFICATION_TYPE,
    NOTIFICATION_CATEGORY : NOTIFICATION_CATEGORY,

    swaggerDefaultResponseMessages: swaggerDefaultResponseMessages,

    HYDRANT_STATUS:HYDRANT_STATUS,
    BLOCK_USER : BLOCK_USER,

    SOCKET_EVENTS : SOCKET_EVENTS,
    STATUS_CODE:STATUS_CODE

};

module.exports = APP_CONSTANTS;
